import { Config } from 'angular-ecmascript/module-helpers';

// Import Templates
import __Chats from '../templates/chats.html';
import __Tabs from '../templates/tabs.html';
 
export default class RoutesConfig extends Config {
  configure() {
    this.$stateProvider
      .state('tab', {
        url: '/tab',
        abstract: true,
        //templateUrl: 'client/templates/tabs.html'
        templateUrl: __Tabs
      })
      .state('tab.chats', {
        url: '/chats',
        views: {
          'tab-chats': {
            //templateUrl: 'client/templates/chats.html',
            templateUrl: __Chats,
            controller: 'ChatsCtrl as chats'
          }
        }
      });
 
    this.$urlRouterProvider.otherwise('tab/chats');
  }
}
 
RoutesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];